use std::fs::File;
use std::io;
use std::io::Read;
use std::io::Write;
use rand::Rng;

fn main() {
    let file = match File::open("/home/yehor/projects/hangman/dictionary.txt"){
        Ok(file) => file,
        Err(e) => {
            println!("No such file.");
            std::process::exit(1);
        },
    };
    
    let mut text = String::new();
    let text: String = match file.read_to_string(&mut text) {
        Ok(text) => text.to_string(),
        Err(e) => {
            println!("Exiting with the following error {e}.");
            std::process::exit(1);
        },
    };
    
    println!("\x1B[2J");
    println!(" _\n| |__   __ _ _ __   __ _ _ __ ___   __ _ _ __\n| '_ \\ / _` | '_ \\ / _` | '_ ` _ \\ / _` | '_ \\\n| | | | (_| | | | | (_| | | | | | | (_| | | | |\n|_| |_|\\__,_|_| |_|\\__, |_| |_| |_|\\__,_|_| |_|\n                |___/");
    let mut cont = true;
    
    while cont != false {
        let mut rword = String::new();
        
        for word in text.lines() {
            if let Ok(&word) = word {
                rword = word.to_string();
                if rand::thread_rng().gen::<bool>() {
                    break;
                }
            }
        }
        
        let secret_word = String::from(rword); // 
        let mut lives = 6;
        let mut cover = vec!('-'; secret_word.len()); 
        
        let mut hangman = String::from("");
        let mut not_inword = String::from("");

        while lives > 0 && cover.iter().collect::<String>() != secret_word {
            println!("Not in the word: {not_inword}\n");
            println!("{hangman}");
            let hangman_parts: Vec<&str> = Vec::from(["||\n", "||    / \\\n", "||    /|\\\n", "||     0\n", "||/    |\n", "________\n"]);
            let mut guess = String::new();
            println!("{}", cover.iter().collect::<String>());
            println!("Try to guess a letter from the word:");
            io::stdin().read_line(&mut guess).expect("Err");
            let guess = guess.trim();
    
            if guess.is_empty() || guess.len() > 1 || !guess.chars().all(char::is_alphabetic) {
                println!("\x1B[2J");
                println!("{hangman}");
                println!("Oops, it seems you haven't enterd a letter.");
                continue;
            }
            
            let guess = guess.trim().chars().next().unwrap();
            
            if not_inword.contains(guess) {
                println!("You have already guessed the letter \"{guess}\" and it is still not in the word.");
            } else {
                for (i, secret_char) in secret_word.chars().enumerate(){
                    if guess == secret_char{
                        cover[i] = guess;
                    }
                }
            
                if !secret_word.contains(guess){
                    lives -= 1;
                    hangman.push_str(hangman_parts[lives]);
                    not_inword.push(guess);
                }
                
                io::stdout().flush().unwrap();
                println!("\x1B[2J");
            }
        }
        if cover.iter().collect::<String>() == secret_word{
            println!("{hangman}");
            println!("{secret_word}");
            println!("You won!");
        } else {
            println!("{hangman}");
            println!("You lost!");
        }
        
        loop{
            let mut answer = String::new();
            println!("Wanna play one more time? [Y/n]: ");
            io::stdin().read_line(&mut answer).expect("Err");
            let answer = answer.trim();
            if !answer.is_empty() && answer != "y" && answer != "Y"{
                match answer.chars().next().unwrap(){
                    'n' => cont = false,
                    _ => continue,
                };
            }
            break;
        }
    }
}
