use rand::prelude::*;
use colored::Colorize;

fn main() {
/* The computer generates a 4 digit code 
    The user types in a 4 digit code. Their guess. 
    The computer tells them how many digits they guessed correctly 
    
    Extension : the computer tells them how many digits are 
    guessed correctly in the correct place and how many digits have 
    been guessed correctly but in the wrong place. 
    
    The user gets 12 guesses to either win – guess the right code. Or 
    lose – run out of guesses. */

    let mut code: Vec<(char, bool)> = Vec::from([
        ('\0', false), 
        ('\0', false), 
        ('\0', false), 
        ('\0', false)
    ]);
    
    for i in 0..4 {
        code[i].0 = char::from_digit(
            thread_rng().gen_range(0..10), 10)
            .unwrap();
    }

    let mut code_str = String::new();
    for i in 0..4 {
        code_str.push(code[i].0);
    }

    //for debugging
    //println!("{:?}", code); 

    println!("Try to break my code");
    let mut attempts = 0;

    while attempts < 12 {
        for i in 0..4 { code[i].1 = false; } //setting all bool values in code tuple to false
        let mut correctly_placed = 0;
        
        //getting a guess
        let guess: Vec<char> = loop {
            println!("\n\nEnter a 4-digit guess or \"help\" for game rules:");
            let mut guess = String::new();
            
            match std::io::stdin().read_line(&mut guess) {
                Ok(str) => str,
                Err(_) => {
                    println!("Make sure to enter a four digit number.");
                    continue;
                },
            };

            if guess.trim().parse::<u16>().is_ok() && guess.trim().len() == 4 {
                break guess.chars().collect::<Vec<char>>();
            } else if guess.trim() == "help" {
                println!("Just simply type a four digit number and press ENTER\nIf the digit is colored green then you've correctly guessed\nthe digit's place and the digit itself.\nIf the digit is colored yellow, then the digit is in the code, but the place is wrong.\nIf it is white, then it is not in the code at all.");
                continue;
            } else {
                println!("Make sure to enter a four digit number or \"help\".");
            }
        };
        
        for i in 0..4 {
            if guess[i] == code[i].0 {
                code[i].1 = true;
                correctly_placed += 1;
                print!("{}", guess[i].to_string().green());
            } else if code.contains(&(guess[i], false)) {
                print!("{}", guess[i].to_string().yellow());
            } else {
                print!("{}", guess[i]);
            }
        }

        if correctly_placed != 4 {
            attempts += 1;
        } else {
            println!("\nYes! You broke the code!");
            break;
        }
    }

    if attempts == 12 {
        println!("\nSorry, you runt out of attempts, the code was {}.", code_str);
    }
}
